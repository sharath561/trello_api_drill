
const { API_KEY, API_TOKEN } = require("./secretData");

function createNewCards(listId, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards?idList=${listId}&key=${API_KEY}&token=${API_TOKEN}`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
        },
      }
    ).then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`)
        return response.json()
    }).then((data)=>{
        resolve(data)
    }).then((error)=>{
        reject(error)
    })
  });
}

module.exports = createNewCards;
//createNewCards(listId, API_KEY, API_TOKEN, cardName);

