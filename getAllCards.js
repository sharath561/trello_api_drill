const { boardId, API_KEY, API_TOKEN, LIST_ID } = require("./secretData");
const getCards = require("./getCards");
const getLists = require("./getLists");

function getAllCards(boardId, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    let listData = [];
    getLists(boardId, API_KEY, API_TOKEN)
      .then((data) => {
        data.forEach((current) => {
          listData.push(getCards(current.id, API_KEY, API_TOKEN));
        });
        //console.log(listData)
        return listData;
      })
      .then((listOfPromises) => {
        //console.log(listOfPromises)
        return Promise.all(listOfPromises);
      })
      .then((promisesData) => {
        resolve(promisesData);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

getAllCards(boardId, API_KEY, API_TOKEN)
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.log(error);
  });
