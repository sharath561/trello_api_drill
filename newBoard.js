//Create a new board, create 3 lists simultaneously, and a card in each list simultaneously

const { boardId, API_KEY, API_TOKEN } = require("./secretData");
const createBoard = require("./createBoard");
const createLists = require("./createLists");
const createCards = require("./createCards");

function createNewBoard(API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    const boardName = "creatingTestingBoard";
    createBoard(API_KEY, API_TOKEN, boardName)
      .then((dataId) => {
        let listIdsData = [];
        for (let index = 1; index <= 3; index++) {
          listIdsData.push(
            createLists(dataId, API_KEY, API_TOKEN, `NewList${index}`)
          );
        }

        return Promise.all(listIdsData);
      })
      .then((listPromiseData) => {
        let cardIdsData = [];
        let listIdSData = [];

        for (let iterate of listPromiseData) {
          const ids = JSON.parse(iterate).id;
          listIdSData.push(ids);

          cardIdsData.push(createCards(ids, API_KEY, API_TOKEN));
        }

        //console.log(listIdSData)

        resolve(listIdSData);

        return Promise.all(cardIdsData);
      })
      .catch((error) => {
        reject(error);
      });
    
  });
}

// createNewBoard(API_KEY, API_TOKEN)
//   .then((data) => {
//     console.log(data);
//   })
//   .catch((error) => console.log(error));

module.exports = createNewBoard;
