const { boardId, API_KEY, API_TOKEN } = require("./secretData");

function getBoard(boardId, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}?key=${API_KEY}&token=${API_TOKEN}`
    )
      .then((responseData) => {
        return responseData.json();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

getBoard(boardId, API_KEY, API_TOKEN)
  .then((boardData) => {
    console.log(boardData);
  })
  .catch((error) => {
    console.log(error);
  });
