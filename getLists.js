const { boardId, API_KEY, API_TOKEN } = require("./secretData");

function getLists(boardId, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?key=${API_KEY}&token=${API_TOKEN}`
    )
      .then((response) => {
        console.log(response.status);
        return response.json();
      })
      .then((responseData) => {
        resolve(responseData);
      })
      .catch((error) => {
        reject(error.message);
      });
  });
}

// getLists(boardId, API_KEY, API_TOKEN)
//   .then((data) => {
//     console.log(data);
//   })
//   .catch((error) => {
//     console.log(error);
//   });

module.exports = getLists;
