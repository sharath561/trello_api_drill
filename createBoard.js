const { boardId, API_KEY, API_TOKEN } = require("./secretData");

function createBoard(API_KEY, API_TOKEN, boardName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${API_KEY}&token=${API_TOKEN}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(response.status)
        return response.json();
      })
      .then((responseData) => {
        resolve(responseData.id);
      })
      .catch((error) => {
        reject(error.message);
      });
  });
}
// createBoard(API_KEY, API_TOKEN, "creatingNewBoard")
//   .then((data) => {
//     console.log(data.id);
//   })
//   .catch((error) => {
//     console.log(error);
//   });
module.exports = createBoard;
