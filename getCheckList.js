const exp = require("constants");
const { API_KEY, API_TOKEN } = require("./secretData");

function updateCheckItem(cardId, checkItemId, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${API_KEY}&token=${API_TOKEN}&state=complete`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.json();
      })
      .then((text) => resolve(text))
      .catch((err) => reject(err));
  });
}

function getItemsData(id, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/checklists/${id}/checkItems?key=${API_KEY}&token=${API_TOKEN}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.json();
      })
      .then((text) => resolve(text))
      .catch((err) => reject(err));
  });
}

function getCheckListsData(cardId, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards/${cardId}/checklists?key=${API_KEY}&token=${API_TOKEN}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.json();
      })
      .then((responseData) => {
        resolve(responseData);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
//"6654183f84e73716c0dab613"

module.exports = { getCheckListsData, getItemsData, updateCheckItem };
