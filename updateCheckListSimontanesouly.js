const { boardId, API_KEY, API_TOKEN } = require("./secretData");
const getLists = require("./getLists");
const getCards = require("./getCards");
const {
  getCheckListsData,
  getItemsData,
  updateCheckItem,
} = require("./getCheckList");

function updateCheckListsim(boardId, API_KEY, API_TOKEN) {
  getLists(boardId, API_KEY, API_TOKEN)
    .then((data) => {
      console.log(data[0].id);
      return data[0].id;
    })
    .then((idData) => {
      getCards(idData, API_KEY, API_TOKEN).then((current) => {
        let cardIdData = current[0].id;

        getCheckListsData(cardIdData, API_KEY, API_TOKEN)
          .then((data) => {
            
            getItemsData(data[0].id, API_KEY, API_TOKEN).then(
              (responseData) => {
                let promisesList = []
                responseData.forEach((current) => {
                  promisesList.push(updateCheckItem(
                    cardIdData,
                    current.id,
                    API_KEY,
                    API_TOKEN
                  ))
                
                })
                return Promise.all(promisesList)
              }).then((promiseResponseData)=>{
                console.log(promiseResponseData)
            });
          })
          .catch((error) => {
            console.log(error);
          });
      });
    }).catch((error)=>{
        console.log(error)
    });
}

updateCheckListsim(boardId, API_KEY, API_TOKEN);
