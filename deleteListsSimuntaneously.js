const { API_KEY, API_TOKEN } = require("./secretData");
const createNewBoard = require("./newBoard");
const archiveList = require("./deleteLists");

function deleteSimuntaneosly() {
  createNewBoard(API_KEY, API_TOKEN)
    .then((data) => {
      let collectionPromise = [];
      data.forEach((ids) => {
        collectionPromise.push(archiveList(ids, API_KEY, API_TOKEN));
      });
      return Promise.all(collectionPromise);
    })
    .then((data) => {
      console.log(data);
    });
}
deleteSimuntaneosly();
