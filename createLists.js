const { API_KEY, API_TOKEN } = require("./secretData");
function createListsOnBoard(boardId, API_KEY, API_TOKEN, listName) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${API_KEY}&token=${API_TOKEN}`,
      {
        method: "POST",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((responseData) => {
        resolve(responseData);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
//createListsOnBoard("6654564dfe0cbd197fda1f42", API_KEY, API_TOKEN,"Sharath");

module.exports = createListsOnBoard;
