const { API_KEY, API_TOKEN } = require("./secretData");

function archiveList(listId, API_KEY, API_TOKEN) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/lists/${listId}/closed?key=${API_KEY}&token=${API_TOKEN}&value=true`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => reject(error));
  });
}

//   createNewBoard(API_KEY,API_TOKEN).then((data)=>{
//       data.forEach((currentIds)=>{
//           // console.log(currentIds)
//           archiveList(currentIds,API_KEY,API_TOKEN)
//       })
//   })
module.exports = archiveList;
